package com.sourceit.lesson15new;

/**
 * Created by Student on 16.01.2018.
 */
public class Generation {
    private Generation() {
    }

    public static Article[] generate(){
        Article[] articles = new Article[10];
        for (int i = 0; i<10; i++) {
            articles[i] = new Article(
                    "Title" +i,
                    "Long long text " +i
            );
        }
        return articles;
    }




}
