package com.sourceit.lesson15new;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArticleAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        adapter = new ArticleAdapter(this, Generation.generate());
        listView.setAdapter(adapter);
    }

    class ArticleAdapter extends ArrayAdapter<Article> {


        public ArticleAdapter(Context context, Article[] articles) {
            super(context, R.layout.item_layout, articles);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View root = getLayoutInflater().inflate(R.layout.item_layout,parent);
            //  View root = super.getView(position, convertView, parent);

            TextView title = (TextView) root.findViewById(R.id.title);
            TextView text = (TextView) root.findViewById(R.id.text);

            Article article = getItem(position);
            title.setText(article.getTitle());
            text.setText(article.getText());
            return root;

        }
    }


}
